#ifndef _PROC_H_
#define _PROC_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>
#include <libproc.h>
#include "vla.h"

#define NAME_LEN 4

struct kinfo_proc *get_all_proc(void);
void append_pid(vla_list head, struct kinfo_proc *proc_list, int proc_count, char **target_ary, int target_ary_len);
char *get_proc_name(pid_t pid);

int PROC_COUNT;

#endif
