#include "vla.h"

vla_list vla_initial(void)
{
	vla_list head = malloc(sizeof(struct vla));
	if (head == NULL)
		return NULL;

	head->proc_name = NULL;
	head->prev = head->next = NULL;

	return head;
}

int vla_append(vla_list head, pid_t value, char *proc_name)
{
	vla_list node = malloc(sizeof(struct vla));
	if (node == NULL)
		return 1;

	node->proc_name = malloc(strlen(proc_name) + 1);
	if (node->proc_name == NULL) {
		fprintf(stderr, "failed to allocate memory\n");
	}
	strcpy(node->proc_name, proc_name);
	node->pid = value;
	node->next = NULL;

	if (head->prev != NULL) {
		head->prev->next = node;
		node->prev = head->prev;
		head->prev = node;
	} else {
		head->next = node;
		head->prev = node;
		node->prev = head;
	}

	vla_len++;

	return 0;
}

vla_list vla_find(vla_list head, pid_t value)
{
	vla_list node = head;

	while ((node = node->next)) {
		if (value == node->pid)
			return node;
	}

	return NULL;
}

int vla_delete(vla_list node)
{
	if (node->prev->next == NULL)
		return 1;

	if (node->prev)
		node->prev->next = node->next;

	if (node->next)
		node->next->prev = node->prev;

	free(node);

	vla_len--;

	return 0;
}

void vla_free(vla_list head)
{
	vla_list node;
	while ((node = head)) {
		head = head->next;
		if (node->proc_name)
			free(node->proc_name);

		free(node);
	}
}

int vla_get_len(void)
{
	return vla_len;
}
