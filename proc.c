#include "proc.h"

struct kinfo_proc *get_all_proc(void)
{
	struct kinfo_proc *process = NULL;
	size_t proc_buf_size;
	int st;
	int name[NAME_LEN] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };

	st = sysctl(name, NAME_LEN, NULL, &proc_buf_size, NULL, 0);
	if (st == -1) {
		return NULL;
	}

	process = malloc(proc_buf_size);
	if (process == NULL) {
		fprintf(stderr, "failed to allocate memory\n");
		exit(1);
	}
	st = sysctl(name, NAME_LEN, process, &proc_buf_size, NULL, 0);
	if (st == -1) {
		fprintf(stderr, "%s\n", strerror(errno));
		exit(1);
	}

	PROC_COUNT = proc_buf_size / sizeof(struct kinfo_proc);

	return process;
}

void append_pid(vla_list head, struct kinfo_proc *proc_list, int proc_count, char **target_ary, int target_ary_len)
{
	char *proc_name;
	pid_t pid;
	int target_index;
	while(--proc_count >= 0) {
		target_index = 0;
		pid = proc_list[proc_count].kp_proc.p_pid;

		if ((proc_name = get_proc_name(pid))){
			while(target_index < target_ary_len) {
				if (strcmp(proc_name, target_ary[target_index]) == 0){
					if (vla_append(head, pid, proc_name) == 1) {
						printf("append pid '%d' to vla_list error\n", pid);
					}
				}
				target_index++;
			}
		}
	}
}

char *get_proc_name(pid_t pid)
{
	char path_buffer[PROC_PIDPATHINFO_MAXSIZE];
	char *proc_name;
	size_t path_len;
	int name_len = 1;
	proc_pidpath(pid, path_buffer, sizeof(path_buffer));
	path_len = strlen(path_buffer);
	if (path_len <= 0)
		return NULL;
	while (--path_len > 0 && path_buffer[path_len] != '/')
		name_len++;
	proc_name = malloc(name_len);
	if (proc_name == NULL) {
		fprintf(stderr, "failed to allocate memory\n");
		exit(1);
	}

	strcpy(proc_name, path_buffer + path_len + 1);

	return proc_name;
}
