#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "proc.h"
#include "vla.h"

extern int PROC_COUNT;

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Usage: kproc <process name> <process name>\n");
		exit(1);
	}
	PROC_COUNT = -1;
	vla_list head = vla_initial();
	struct kinfo_proc *proc_list = get_all_proc();
	if (proc_list == NULL) {
		fprintf(stderr, "get process list error\n");
		exit(1);
	}

	append_pid(head, proc_list, PROC_COUNT, argv + 1, argc - 1);
	vla_list node = head->next;
	while(node) {
		printf("%d\t%s\n", node->pid, node->proc_name);
		if (kill(node->pid, 9) == -1) {
			fprintf(stderr, "kill process: %s error\nerror message: %s\n", node->proc_name, strerror(errno));
		}
		node = node->next;
	}
	vla_free(head);
	return 0;
}
