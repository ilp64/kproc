kproc: vla.o proc.o main.o
	gcc vla.o proc.o main.o -o kproc

vla.o: vla.c vla.h
	gcc -c vla.c

proc.o: proc.c proc.h vla.c vla.h
	gcc -c proc.c

main.o: main.c
	gcc -c main.c

clean:
	rm *.o kproc

install:
	cp kproc /usr/local/bin
