#ifndef _VLA_H_
#define _VLA_H_

#include <string.h>
#include <stdlib.h>
#include <stdio.h>



typedef struct vla {
	pid_t pid;
	char *proc_name;

	struct vla *prev;
	struct vla *next;
} *vla_list;

vla_list vla_initial(void);
int vla_append(vla_list head, pid_t value, char *proc_name);
vla_list vla_find(vla_list head, pid_t value);
int vla_delete(vla_list node);
void vla_free(vla_list head);

static int vla_len = 0;

#endif
